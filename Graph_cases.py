""" Program that prints graphs for the cumulative cases and new cases over time """

import numpy as np
import matplotlib.pyplot as plt
import csv

#Arrays to store the cumulative and new cases
cum_cases = []
new_cases = []

#Reads the CSV file and adds data to the arrays
with open("data/Yorkshire_and_The_Humber.csv") as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    count = 0
    for row in reader:
        if count == 0:
            count += 1
        else:
            cum_cases.append(int(row[2]))
            new_cases.append(int(row[3]))

#Information for the graphs
days = 266
region = 'Yorkshire and The Humber'
time = np.linspace(0,days,days)
font = {'fontname':'Times New Roman'}
plt.rc('font', family='Arial')

#Plots the cumulative cases graph
plt.plot(time, cum_cases, 'k')
#Plots indicators of each event
plt.axvline(x=138, alpha=0.6, color='g', label='Tiered system starts', linestyle='--')
plt.axvline(x=160, alpha=0.6, color='m', label='Second lockdown starts', linestyle='--')
plt.axvline(x=187, alpha=0.6, color='r', label='Second lockdown ends', linestyle='--')
plt.axvline(x=222, alpha=0.6, color='b', label='Third lockdown starts', linestyle='--')
#Formats the graph
ax = plt.gca()
ax.set_facecolor('darkgrey')
plt.legend(fontsize=10.5)
plt.grid(True, color='grey')
plt.xlabel('Days since 29/05/20', **font, fontsize=13)
plt.ylabel('Cases', **font, fontsize=13)
plt.title('Cumulative cases for ' +region, **font, fontsize=16, pad=12)
plt.show()

#Plots the new cases graph
plt.plot(time, new_cases, 'k')
#Plots indicators of each event
plt.axvline(x=138, alpha=0.6, color='g', label='Tiered system starts', linestyle='--')
plt.axvline(x=160, alpha=0.6, color='m', label='Second lockdown starts', linestyle='--')
plt.axvline(x=187, alpha=0.6, color='r', label='Second lockdown ends', linestyle='--')
plt.axvline(x=222, alpha=0.6, color='b', label='Third lockdown starts', linestyle='--')
#Formats the graph
ax = plt.gca()
ax.set_facecolor('darkgrey')
plt.legend(fontsize=10.5)
plt.grid(True, color='grey')
plt.xlabel('Days since 29/05/20', **font, fontsize=13)
plt.ylabel('Cases', **font, fontsize=13)
plt.title('New cases for ' +region, **font, fontsize=16, pad=12)
plt.show()