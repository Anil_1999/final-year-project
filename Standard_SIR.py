""" Program that prints graphs to compare real cases vs predicted cases with the SIR Model """

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import csv

### Data about regions in form 'Region, N, I0, R0'###
England = ['England', 55980000, 16310, 200315]
East_England = ['East England', 6235000, 1704, 19196]
London = ['London', 8982000, 838, 31410]
North_East = ['North East', 2657000, 903, 13247]
North_West = ['North West', 7300000, 3354, 33783]
East_Midlands = ['East Midlands', 4811000, 2016, 14049]
West_Midlands = ['West Midlands', 2928000, 1753, 20941]
South_East = ['South East', 9175000, 2096, 28554]
South_West = ['South West', 5616000, 841, 11063]
Yorkshire = ['Yorkshire and The Humber', 5486000, 2597, 20776]

#Function used to return the derivatives 
def derivative(y, t, N, b, k):
    S, I, R, C = y
    dS = -b * S * I   #Susceptible equation
    dI = b * S * I - k * I   #Infected equation
    dR = k * I   #Recovered equation
    dC = -dS  #Cumulative infections equation
    return dS, dI, dR, dC

#Arrays to store the cases and r rates
r_cases = []
r_rate = []
p_cases = []

#Reads the CSV file and adds entries to the real cases array
with open("data/North_West.csv") as csvfile1:
    reader1 = csv.reader(csvfile1, delimiter=',')
    count1 = 0
    for row in reader1:
        if count1 == 0: #first line is data types
            count1 += 1
        else:
            r_cases.append(float(row[2]))

#Reads the CSV file and adds the r rates to the r rates array
with open("data/R_Rates.csv") as csvfile2:
    reader2 = csv.reader(csvfile2, delimiter=',')
    count2 = 0
    for row in reader2:
        if count2 == 0:
            count2 += 1
        else:
            r_rate.append(float(row[5]))

#Information required for the graphs and equations  
days = 266
tmp = North_West
region = tmp[0]
N = tmp[1]
I = tmp[2]
R = tmp[3]
S = N - I - R
C = N - S
k = 1/6
y0 = S/N, I/N, R/N, C/N
font = {'fontname':'Times New Roman'}
plt.rc('font', family='Arial')

#Loops through the weeks and passes the correct data to the differential equation solver
for x in range(len(r_rate)):
    #Cases for the first week
    if x==0:
        t = np.linspace(0, 6, 7)
        b = r_rate[x] * k
        grid = odeint(derivative, y0, t, args=(N, b, k))
        S, I, R, C = grid.T
        y0 = S[-1], I[-1], R[-1], C[-1]
        for i in range(len(C)):
            p_cases.append(float(N*C[i]))
    #Cases for all the following weeks
    else:
        t = np.linspace(7*x, (7*x) + 7, 8)
        b = r_rate[x] * k
        grid = odeint(derivative, y0, t, args=(N, b, k))
        S, I, R, C = grid.T
        y0 = S[-1], I[-1], R[-1], C[-1]
        for i in range(len(C)):
            #First cases will be the same as the last cases from the previous week so we don't include it
            if i!=0:
                p_cases.append(float(N*C[i]))
    
#Plots the predicted and real cases
time = np.linspace(0,days,days)
#plt.figure(dpi=200)
plt.plot(time, r_cases, 'k', label='Real cases', linewidth = 2.0)
plt.plot(time, p_cases, 'w', label='Predicted cases', linewidth = 2.0)
#Plots indicators of each event
plt.axvline(x=138, alpha=0.6, color='g', label='Tiered system starts', linestyle='--')
plt.axvline(x=160, alpha=0.6, color='m', label='Second lockdown starts', linestyle='--')
plt.axvline(x=187, alpha=0.6, color='r', label='Second lockdown ends', linestyle='--')
plt.axvline(x=222, alpha=0.6, color='b', label='Third lockdown starts', linestyle='--')
#Formats the graph
ax = plt.gca()
ax.set_facecolor('darkgrey')
plt.legend(fontsize=10.5)
plt.grid(True, color='grey')
plt.xlabel('Days since 29/05/20', **font, fontsize=13)
plt.ylabel('Cumulative cases', **font, fontsize=13)
plt.title('Case Predictions for ' +region +' (Standard and k=1/6)', **font, fontsize=16, pad=12)
plt.tight_layout()
save1 = tmp[0] + ".png"
plt.savefig(save1, dpi=350)
plt.show()

#Plots the predicted and real cases in a log graph
time = np.linspace(0,days,days)
plt.plot(time, r_cases, 'k', label='Real cases', linewidth = 2.0)
plt.plot(time, p_cases, 'w', label='Predicted cases', linewidth = 2.0)
#Plots indicators of each event
plt.axvline(x=138, alpha=0.6, color='g', label='Tiered system starts', linestyle='--')
plt.axvline(x=160, alpha=0.6, color='m', label='Second lockdown starts', linestyle='--')
plt.axvline(x=187, alpha=0.6, color='r', label='Second lockdown ends', linestyle='--')
plt.axvline(x=222, alpha=0.6, color='b', label='Third lockdown starts', linestyle='--')
#Formats the graph
ax = plt.gca()
ax.set_yscale('log')
ax.set_facecolor('darkgrey')
plt.legend(fontsize=10.5)
plt.grid(True, color='grey')
plt.xlabel('Days since 29/05/20', **font, fontsize=13)
plt.ylabel('Cumulative cases', **font, fontsize=13)
plt.title('Case Predictions for ' +region +' (Standard and k=1/6)', **font, fontsize=16, pad=12)
plt.tight_layout()
save2 = tmp[0] + " " + " (log)" + ".png"
plt.savefig(save2, dpi=350)
plt.show()