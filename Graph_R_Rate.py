""" Program that prints graphs of the r rate over time """

import numpy as np
import matplotlib.pyplot as plt
import csv
from scipy.interpolate import make_interp_spline, BSpline

#Arrays to store r rates for each week
england = []
east_england = []
london = []
north_east = []
north_west = []
midlands = []
south_east = []
south_west = []
yorkshire = []

#Reads the CSV file and adds the data to the arrays
with open("data/R_Rates.csv") as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    count = 0
    for row in reader:
        if count == 0: #first line is data types
            count += 1
        else:
            england.append(float(row[1]))
            east_england.append(float(row[2]))
            london.append(float(row[3]))
            north_east.append(float(row[4]))
            north_west.append(float(row[5]))
            midlands.append(float(row[6]))
            south_east.append(float(row[7]))
            south_west.append(float(row[8]))
            yorkshire.append(float(row[9]))

#Store each regions r rates as an array
r_rates = [east_england,london,north_east,north_west,midlands,south_east,south_west,yorkshire]
#Store each regions name as an array
regions = ['East England', 'London', 'North East', 'North West', 'Midlands', 'South East', 'South West', 'Yorkshire and The Humber']

#Formatting for the graph
font = {'fontname':'Times New Roman'}
plt.rc('font', family='Arial')

#Creates a graph of the England r rate over time
save = 'England'
fig = plt.figure()
ax = fig.add_subplot()
t = np.arange(38)
xnew = np.linspace(t.min(), t.max(), 300)
espl = make_interp_spline(t, england, k=3) #used to make England's r rate line smooth
eSmooth = espl(xnew)
ax.set_facecolor('darkgrey')
ax.plot(xnew, eSmooth, 'k', label = 'England Average') #plot england's r rate
plt.axvline(x=20, alpha=0.6, color='g', label='Tiered system starts', linestyle='--')
plt.axvline(x=23, alpha=0.6, color='m', label='Second lockdown starts', linestyle='--')
plt.axvline(x=27, alpha=0.6, color='r', label='Second lockdown ends', linestyle='--')
plt.axvline(x=32, alpha=0.6, color='b', label='Third lockdown starts', linestyle='--')
ax.legend(fontsize=9)
plt.grid(True, color='grey') #add a grid to the graph
ax.set_xlabel('Weeks since 29/05/2020', **font, fontsize=13)
ax.set_ylabel('R-rate', **font, fontsize=13)
plt.title('R-rate for England', **font, fontsize=16, pad=12)
ax.set_ylim([0.6,1.6])
plt.tight_layout()
plt.savefig(save, dpi=350)
plt.show()

#Loops through each region and creates a graph of their r rate over time
for x in range(8):
    save = regions[x]
    fig = plt.figure()
    ax = fig.add_subplot()
    t = np.arange(38)
    xnew = np.linspace(t.min(), t.max(), 300)
    spl = make_interp_spline(t, r_rates[x], k=3) #used to make the region r rate line smooth
    smooth = spl(xnew)
    espl = make_interp_spline(t, england, k=3) #used to make England's r rate line smooth
    eSmooth = espl(xnew)
    ax.set_facecolor('darkgrey')
    ax.plot(xnew, eSmooth, 'k', label = 'England Average') #plot england's r rate
    ax.plot(xnew, smooth, 'w', label = regions[x]) #plot the regions r rate
    plt.axvline(x=20, alpha=0.6, color='g', label='Tiered system starts', linestyle='--')
    plt.axvline(x=23, alpha=0.6, color='m', label='Second lockdown starts', linestyle='--')
    plt.axvline(x=27, alpha=0.6, color='r', label='Second lockdown ends', linestyle='--')
    plt.axvline(x=32, alpha=0.6, color='b', label='Third lockdown starts', linestyle='--')
    ax.legend(fontsize=9)
    plt.grid(True, color='grey') #add a grid to the graph
    ax.set_xlabel('Weeks since 29/05/2020', **font, fontsize=13)
    ax.set_ylabel('R-rate', **font, fontsize=13)
    plt.title('R-rate for ' + regions[x], **font, fontsize=16, pad=12)
    ax.set_ylim([0.6,1.6])
    plt.tight_layout()
    plt.savefig(save, dpi=350)
    plt.show()


