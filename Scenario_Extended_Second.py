""" Progream to model the event of an extended second lockdown """

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import csv

#Function used to return the derivatives 
def derivative(y, t, N, b, k):
    S, I, R, C = y
    dS = -b * S * I  #Susceptible equation
    dI = b * S * I  - k * I   #Infected equation
    dR = k * I   #Recovered equation
    dC = -dS  #Cumulative infections equation
    return dS, dI, dR, dC

#Function used to return the derivatives  for superspreading
def sderivative(y, t, N, b, k, spreader, sb):
    S, I, R, C = y
    dS = -b * S * I * (1 - spreader) -sb * S * I * spreader   #Susceptible equation
    dI = b * S * I * (1 - spreader) + sb * S * I * spreader - k * I   #Infected equation
    dR = k * I   #Recovered equation
    dC = -dS  #Cumulative infections equation
    return dS, dI, dR, dC

#Arrays to store the cases and r rates
r_cases = []
hp_cases = []
lp_cases = []
mp_cases = []
r_rate = []

#Reads the CSV file and adds entries to the real cases array
with open("data/England.csv") as csvfile1:
    reader1 = csv.reader(csvfile1, delimiter=',')
    count1 = 0
    for row in reader1:
        if count1<161 or count1>223: #first line is data types
            count1 += 1
        else:
            r_cases.append(float(row[2]))
            count1 += 1

#Reads the CSV file and adds the r rates to the r rates array
with open("data/R_Rates.csv") as csvfile2:
    reader2 = csv.reader(csvfile2, delimiter=',')
    count2 = 0
    for row in reader2:
        if count2<24 or count2>27:
            count2 += 1
        else:
            r_rate.append(float(row[1]))
            count2 += 1
 
#Information required for the graphs and equations             
days = 63
region = 'England'
N = 8982000
I = 139209
R = 870009
S = N - I - R
C = N - S
k = 1/10
ytmp = S/N, I/N, R/N, C/N
spreader = 0.05
rplus = 0.35
font = {'fontname':'Times New Roman'}
plt.rc('font', family='Arial')

#Loops thorugh an alternative timeline where there is no second lockdown and superspreading is still active
for x in range(int(days/7)):
    #Cases for the first week
    if x==0:
        y0 = ytmp
        t = np.linspace(0,6,7)
        b = r_rate[x]*k
        grid = odeint(derivative, y0, t, args=(N, b, k))
        S, I, R, C = grid.T
        hy = S[-1], I[-1], R[-1], C[-1]
        ly = S[-1], I[-1], R[-1], C[-1]
        my = S[-1], I[-1], R[-1], C[-1]
        for i in C:
            hp_cases.append(float(N*i))
            lp_cases.append(float(N*i))
            mp_cases.append(float(N*i))
    #Cases for real lockdown duration
    elif x<4:
        t = np.linspace(7*x, (7*x) + 7, 8)
        b = r_rate[x]*k
        grid = odeint(derivative, hy, t, args=(N, b, k))
        S, I, R, C = grid.T
        hy = S[-1], I[-1], R[-1], C[-1]
        ly = S[-1], I[-1], R[-1], C[-1]
        my = S[-1], I[-1], R[-1], C[-1]
        for i in range(len(hC)):
            if i!= 0:
                hp_cases.append(float(N*C[i]))
                lp_cases.append(float(N*C[i]))
                mp_cases.append(float(N*C[i]))
    #Cases for extended lockdown duration
    elif x>3 and x<8:
        t = np.linspace(7*x, (7*x) + 7, 8)
        hb = r_rate[3]*k + x*0.005
        lb = r_rate[3]*k - x*0.005
        mb = r_rate[3]*k
        hgrid = odeint(derivative, hy, t, args=(N, hb, k))
        lgrid = odeint(derivative, ly, t, args=(N, lb, k))
        mgrid = odeint(derivative, my, t, args=(N, mb, k))
        hS, hI, hR, hC = hgrid.T
        lS, lI, lR, lC = lgrid.T
        mS, mI, mR, mC = mgrid.T
        hy = hS[-1], hI[-1], hR[-1], hC[-1]
        ly = lS[-1], lI[-1], lR[-1], lC[-1]
        my = mS[-1], mI[-1], mR[-1], mC[-1]
        for i in range(len(hC)):
            if i!= 0:
                hp_cases.append(float(N*hC[i]))
                lp_cases.append(float(N*lC[i]))
                mp_cases.append(float(N*mC[i]))
    #Cases for 1 week after our lockdown ends
    else:
        t = np.linspace(7*x, (7*x) + 7, 8)
        hb = r_rate[3]*k + x*0.005
        lb = r_rate[3]*k - x*0.005
        mb = r_rate[3]*k
        hsb = hb + rplus
        lsb = lb + rplus
        msb = b + rplus
        hgrid = odeint(sderivative, hy, t, args=(N, hb, k, spreader, hsb))
        lgrid = odeint(sderivative, ly, t, args=(N, lb, k, spreader, lsb))
        mgrid = odeint(sderivative, my, t, args=(N, mb, k, spreader, msb))
        hS, hI, hR, hC = hgrid.T
        lS, lI, lR, lC = lgrid.T
        mS, mI, mR, mC = mgrid.T
        hy = hS[-1], hI[-1], hR[-1], hC[-1]
        ly = lS[-1], lI[-1], lR[-1], lC[-1]
        my = mS[-1], mI[-1], mR[-1], mC[-1]
        for i in range(len(hC)):
            if i!= 0:
                hp_cases.append(float(N*hC[i]))
                lp_cases.append(float(N*lC[i]))
                mp_cases.append(float(N*mC[i]))

#Plots the predicted and real cases for 56 days
time = np.linspace(0,days,days)
plt.plot(time, r_cases, 'k', label='Real cases', linewidth = 2.0)
plt.plot(time, hp_cases, 'r', label='Upper bound predictions', linewidth = 1.5)
plt.plot(time, mp_cases, 'b', label='Mean predictions', linewidth = 1.5)
plt.plot(time, lp_cases, 'g', label='Lower bound predictions', linewidth = 1.5)
plt.fill_between(time, hp_cases, lp_cases, color = 'w', label='Range of predictions')
#Plots the indicator events
plt.axvline(x=28, alpha=0.6, color='m', label='Real lockdown ends', linestyle='--')
plt.axvline(x=56, alpha=0.6, color='gold', label='Our lockdown ends', linestyle='--')
#Formats the graph
ax = plt.gca()
ax.set_facecolor('darkgrey')
plt.legend(fontsize=9.5, loc='upper left')
plt.grid(True, color='grey')
plt.xlabel('Days since 5/11/20', **font, fontsize=13)
plt.ylabel('Cumulative cases', **font, fontsize=13)
plt.title('Case Predictions for Extended Second Lockdown (63 days)', **font, fontsize=16, pad=12)
plt.tight_layout()
plt.savefig('63',dpi=350)
plt.show()